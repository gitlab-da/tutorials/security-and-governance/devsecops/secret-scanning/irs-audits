# IRS Audits

This project contains fake Social Security Numbers (SSN) used for testing GitLab's secret scanning capabilities.

The .gitlab-ci.yml file adds Secret Detection to the CI/CD pipeline to detect vulnerabilities before they make it into production. The secret detection scanner uses the Custom IRS Rules project to remotely override the scanner's output for the detected secrets.

## References

- [Secret Scanning](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/)

- [Remote Rulesets](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/customize_rulesets.html)

- Project photo by [Olga DeLawrence](https://unsplash.com/@walkingondream?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/black-and-silver-pen-on-white-paper-5616whx5NdQ?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)